/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Punto;
import javax.swing.JOptionPane;

/**
 *
 * http://www.aprendeaprogramar.com/mod/resource/view.php?id=462
 * @author madarme
 */
public class PruebaPunto_Caputura_Excep {
    public static void main(String[] args) {
        
        try{
        Punto p1=crearPunto();
        JOptionPane.showMessageDialog(null, "El punto fue:"+p1.toString());
        }catch(java.lang.NullPointerException e)
        {
        JOptionPane.showMessageDialog(null, "No se puede obtener valores el objeto punto es null");
        }
        
             
        
    }
    
    
    
    public static Punto crearPunto()
    {
        Punto p1=new Punto();
        //Leer datos:
        String datoX=JOptionPane.showInputDialog(null, "Digite punto x","Punto x", JOptionPane.QUESTION_MESSAGE);
        
        /**
         * Validar forma antigua:
         *  if datoX No es un float
         *      --> envíe mensaje de error
         * else
         *    solicite datoY y valide de nuevo
         * 
         */
        String datoY=JOptionPane.showInputDialog(null, "Digite punto y","Punto y", JOptionPane.QUESTION_MESSAGE);
        //Clases envoltorios:
        // Si todo está bien
        try{ 
            float x=Float.parseFloat(datoX);
            float y=Float.parseFloat(datoY);
            p1.setX(x);
            p1.setY(y);
            return p1;
        }catch(java.lang.NumberFormatException e) // Si algo pasa
        {
        
            System.err.println("Se esperaba datos float");
        }
        return null;
    }
    
    
    
    
}
