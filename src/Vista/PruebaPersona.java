/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Persona;

import javax.swing.JOptionPane;

/**
 * Su tarea consiste en crear dos objetos de la clase Persona y probar todos los
 * requisitos
 *
 * @author madar
 */
public class PruebaPersona {

    public static void main(String[] args) {
        // :) good luck !!!
        // I am sure you will do your BEST WORK !!!

        try {
            //requisito 1
            String persona1 = JOptionPane.showInputDialog("CC, Nombre, fechaNac (dd/mm/yyyy) De la Persona1");
            Persona p1 = new Persona(persona1);

            String persona2 = JOptionPane.showInputDialog("CC, Nombre, fechaNac (dd/mm/yyyy) De la Persona2");
            Persona p2 = new Persona(persona2);
            //requisito 2
            System.out.println("El nombre de la persona 1 es: " + p1.getNombre());
            System.out.println("El nombre de la persona 2 es: " + p2.getNombre());

            //requisito 3
            if (p1.equals(p2)) {
                System.out.println("La persona numero 1 es igual a la persona numero 2");
            } else {
                System.out.println("La persona numero 1 No es igual a la persona numero 2");
            }

            //requisito 4
            int i = ((Comparable)p1).compareTo(p2);
            if (i < 0) {
                System.out.println(p1.getNombre() + " es mayor que " + p2.getNombre());
            } 
            if (i > 0) {
                System.out.println(p2.getNombre() + " es mayor que " + p1.getNombre());
            }
            if(i == 0){
                System.out.println(p1.getNombre() + " tiene la misma edad que " + p2.getNombre());
            }

            //requisito 5
            System.out.println(p1.toString());
            System.out.println(p2.toString());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
