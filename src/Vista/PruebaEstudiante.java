/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Estudiante;
import javax.swing.JOptionPane;

/** 
 *
 * @author madar
 */
public class PruebaEstudiante {

    public static void main(String[] args) {
        try {
            // Requisito 1
            String datos1 = JOptionPane.showInputDialog("Ingresa El Codigo y El Nombre Del Estudiante1");
            Estudiante x = new Estudiante(datos1);

            String datos2 = JOptionPane.showInputDialog("Ingresa El Codigo y El Nombre Del Estudiante2");
            Estudiante y = new Estudiante(datos2);

            //Utilizar requisito 2:
            System.out.println("El nombre del objeto x es:" + x.getNombre());
            //Utilizar requisito 3:
            if (x.equals(y)) {
                System.out.println("El objeto x es igual al objeto y");
            } else {
                System.out.println("El objeto x NO es igual al objeto y");
            }
            //Utilizar requisito 4:
            int comparador = ((Comparable) x).compareTo(y);
            String msg = "Son iguales x e y";
            switch (comparador) {
                case 1: {
                    msg = "X es mayor que Y";
                    break;
                }
                case -1:
                    msg = "X es menor que Y";
                    break;//
            }

            System.out.println(msg);

            //Requisito 5:
            System.out.println(x.toString());
            System.out.println(y.toString());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

}
