/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 * Su tarea consite en crear los 5 requisitos tomando en cuenta que su factor de
 * comparación es la FECHA DE NACIMIENTO
 *
 * @author madar
 */
public class Persona implements Comparable {

    private long cedula;
    private String nombre;
    private short diaNacimiento, mesNacimiento, agnoNacimiento;

    //Requisito 1:
    public Persona() {
    }

    public Persona(long cedula, String nombre, short diaNacimiento, short mesNacimiento, short agnoNacimiento) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.diaNacimiento = diaNacimiento;
        this.mesNacimiento = mesNacimiento;
        this.agnoNacimiento = agnoNacimiento;
    }

    public Persona(String datos) throws Exception {

        /**
         * COMPROBAMOS QUE LE STRING NO ESTE VACIO
         */
        if (datos == null || datos.isEmpty()) {
            throw new Exception("Alguna de las cadenas esta vacia ");
        }

        /*
        ASEGURAMOS QUE EL FORMATO ESTE BIEN INGRESADO
         */
        String atributos[] = datos.split(",");
        if (atributos.length != 3) {
            throw new Exception("El numero de datos no es correcto");
        }

        /**
         * INICIALIZACION DEL NOMBRE
         */
        this.nombre = atributos[1];

        /**
         * COMPROBAMOS QUE EL NUMERO DE CEDULA SEA CORRECTO / INICIALIZACION DE
         * CEDULA
         */
        try {
            this.cedula = Integer.parseInt(atributos[0]);
        } catch (Exception e) {
            throw new Exception("El numero de documento de " + this.getNombre() + " no es valido");
        }

        /*
        COMPROBACION DEL FORMATO DE LA FECHA
         */
        String fechas[] = atributos[2].split("/");
        if (fechas.length != 3) {
            throw new Exception("La fecha de nacimento de " + this.getNombre() + " no esta en el formato correcto");
        }

        try {
            /**
             * AÑO DE NACIMIENTO
             */
            this.agnoNacimiento = Short.parseShort(fechas[2]);

            /**
             * MES DE NACIMIENTO
             */
            this.mesNacimiento = Short.parseShort(fechas[1]);

            /**
             * DIA DE NACIMIENTO
             */
            this.diaNacimiento = Short.parseShort(fechas[0]);

        } catch (Exception e) {
            throw new Exception("La fecha de nacimento de " + this.getNombre() + " no esta en el formato correcto");
        }

        /*
        COMPROBAMOS QUE LA FECHA SEA VALIDA
         */
        if (!comprobarFecha(this.agnoNacimiento, this.mesNacimiento, this.diaNacimiento)) {
            throw new Exception("La fecha de nacimento de " + this.getNombre() + " no es valida");
        }
    }

    //Requisito 2:
    public long getCedula() {
        return cedula;
    }

    public void setCedula(long cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public short getDiaNacimiento() {
        return diaNacimiento;
    }

    public void setDiaNacimiento(short diaNacimiento) {
        this.diaNacimiento = diaNacimiento;
    }

    public short getMesNacimiento() {
        return mesNacimiento;
    }

    public void setMesNacimiento(short mesNacimiento) {
        this.mesNacimiento = mesNacimiento;
    }

    public short getAgnoNacimiento() {
        return agnoNacimiento;
    }

    public void setAgnoNacimiento(short agnoNacimiento) {
        this.agnoNacimiento = agnoNacimiento;
    }

    //Requisito 3:
    @Override
    public boolean equals(Object obj) {
        boolean salida = false;
        if (this == obj) {
            salida = true;
        }
        if (obj == null) {
            salida = false;
        }
        if (getClass() != obj.getClass()) {
            salida = false;
        }
        final Persona other = (Persona) obj;

        if ((this.diaNacimiento == other.getDiaNacimiento()) && (this.mesNacimiento == other.getMesNacimiento()) && (this.agnoNacimiento == other.getAgnoNacimiento())) {
            salida = true;
        }
        return salida;
    }

    //Requisito 4:
    @Override
    public String toString() {
        return "Persona{" + "cedula = " + cedula + ", nombre = " + nombre + ", diaNacimiento = " + diaNacimiento + ", mesNacimiento = " + mesNacimiento + ", agnoNacimiento = " + agnoNacimiento + '}';
    }

    //Requisito 5:
    @Override
    public int compareTo(Object obj) {
        if (this == obj) { // Compara si los dos objetos están apuntando a la misma dirección de memoria-> los objetos son iguales
            return 0;
        }
        final Persona other = (Persona) obj;
        int i = 0;
        i = (this.agnoNacimiento * 10000 + this.mesNacimiento * 100 + this.diaNacimiento) - (other.agnoNacimiento * 10000 + other.mesNacimiento * 100 + other.diaNacimiento);
        return i;

    }

    /*
    Este metodo confirma la validez de una fecha
     */
    public boolean comprobarFecha(short agno, short mes, short dia) {
        int[] dias = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        if (mes == 2 && (agno % 4 == 0) && ((agno % 100 != 0 || agno % 400 == 0))) {
            dias[1] = 29;
        }
        boolean salida = dia > 0 && agno > 0 && mes > 0 && mes < 13 && dia <= dias[mes - 1];
        return salida;

    }
}
